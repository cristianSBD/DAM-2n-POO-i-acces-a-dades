#### Herència

L'*herència* és un mecanisme que ens permet la reutilització de codi de
classes existents a base de modificar-lo i ampliar-lo en les classes
derivades. Una classe que deriva d'una altra hereta els atributs i
comportament de la classe base.

L'herència és una forma d'estructurar i reutilitzar el codi. La idea és
que una classe pot heretar o derivar d'una altra classe, cosa que
significa que automàticament pot fer ús de les seves variables i
mètodes, i pot ampliar el conjunt de membres disponible, o modificar-ne
alguns.

Aquest mecanisme ens permet reutilitzar el codi d'una forma molt més
pràctica que amb les funcions de la programació estructurada, i
evidentment que el copiar i enganxar codi, perquè és més flexible als
petits canvis que necessitem fer, que podrem aplicar a la classe
derivada sense necessitat de modificar la classe superior.

És convenient utilitzar l'herència quan podem dir que un objecte de la
classe derivada és un objecte de la classe superior. Per exemple, com
que un gos és un animal, tindria sentit dir que la classe *Gos* deriva
de la classe *Animal*. En canvi, evitarem l'herència en aquells casos en
què la relació sigui “*s'assembla*” en comptes de “*és*”. Per exemple,
si ja tenim la classe *Animal* creada, i ara hem de crear la classe
*Planta*, com que tenen característiques comunes (neixen, creixen,
s'alimenten, moren, es reprodueixen...), podríem estar temptats a
derivar la classe *Planta* de la classe *Animal*. Això sol ser un error
de disseny que pot tenir conseqüències negatives en el manteniment del
programa. En aquest cas, seria convenient definir una classe o
interfície *EsserViu*, amb aquestes característiques comunes, i d'aquí
derivar-ne tant *Animal* com *Planta*.

L'herència s'utilitza per tant quan es detecta una relació que es pot
expressar com “*és un*” entre dues classes. Per exemple, podríem tenir
una classe base *Forma* i que d'ella en derivessin les classes *Cercle*,
*Quadrat* i *Triangle*:

![herencia](docs/utilitzacio_avancada_de_classes/imatges/herencia.png)

A les classes *Cercle*, *Quadrat* i *Triangle* hem sobreescrit els
mètodes *dibuixa()* i *esborra()* per adaptar-los a cadascuna de les
formes. La resta de mètodes no s'han sobreescrit a les classes
derivades, perquè el comportament definit a *Forma* ja ens servia.

En Java, **una classe només pot derivar-ne d'una altra, mai de dues o
més. En canvi, sí que pot implementar tantes interfícies com calgui**.
Tot i que l'herència múltiple és útil en alguns casos, també condueix a
complexitats de disseny i és propens a errors, i per això es va preferir
no incloure-la en el llenguatge.

Tota classe per la qual no s'especifiqui de qui deriva, deriva
automàticament de la classe *Object*.