Configuració per accedir al GitLab amb ssh des de Windows
------------------------

Primer hem de fer un fork del projecte mestre al nostre Gitlab.

Després, accedim a la **consola de git bash.**

[Video tutorial](https://about.gitlab.com/2014/03/04/add-ssh-key-screencast/)

***Comandaments:***

```bash
ssh-keygen -t rsa -C "nombrecorreo"  # Això genera la parella de claus
cat ~/.ssh/id_rsa.pub  # Mostra per pantalla la clau pública
```

La ruta per la parella de claus la pot decidir cada usuari.

```git
git clone git@gitlab.com:NomCompte/RutaRepositori.git
```

**Exemple**: *git@gitlab.com:juanmg2/DAM-2n-POO-i-acces-a-dades.git*
