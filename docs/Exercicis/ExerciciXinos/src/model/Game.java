package model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public class Game {
	// Constant per facilitar el canvi en el nombre de jugadors
	public static final int N_PLAYERS = 4;
	public enum State {
		STATE_HIDE_COINS, STATE_BID, STATE_SHOW_RESULT, STATE_END_GAME;

		public State next() {
			return values()[(ordinal()+1)%4];
		}
	};
	// Estat actual del joc
	/* TODO: crea un SimpleObjectProperty per inicialitzar l'atribut state.
	 * L'estat inicial del joc �s STATE_HIDE_COINS
	 */
	private ObjectProperty<State> state = new SimpleObjectProperty<State>(State.STATE_HIDE_COINS);

	private Player[] players = new Player[N_PLAYERS];

	public Game() {
		for (int i=0; i<N_PLAYERS; i++) {
			players[i] = new Player();
		}
	}

	/*
	 * TODO: retorna l'estat actual del joc.
	 */
	public State getState() {
		return state.get();
	}

	public ObjectProperty<State> getStateProperty() {
		return state;
	}

	/*
	 * TODO: mou l'estat del joc al seg�ent estat.
	 * Utilitza el m�tode next() de l'enum State.
	 */
	public void nextState() {
		state.set(getState().next());
	}

	public void setHiddenCoins(int hiddenCoins) {
		players[0].setHiddenCoins(hiddenCoins);
		for (int i=1; i<players.length; i++) {
			players[i].hide();
		}
	}

	public int getHiddenCoinsCount() {
		int count = 0;
		/*
		 * TODO: Calcula el total de monedes que han
		 * amagat els jugadors.
		 */
		for (int i = 0; i<players.length; i++){
			count+= players[i].getHiddenCoins();

		}

		return count;
	}

	public Player getPlayer(int nPlayer) {
		return players[nPlayer];
	}

	public int getGuanyador() {
		// Total de monedes amagades
		int count = getHiddenCoinsCount();

		// Determinem qui ha guanyat
		int winnerPos=0;
		/*
		 * TODO: determina quin jugador ha encertat la
		 * quantitat de monedes amagades. Si m�s d'un ha
		 * encertat, nom�s cal que retornis el primer que
		 * trobis. Si cap jugador ha encertat, retorna -1.
		 */
		for (int i=0; i<players.length; i++){
			if (players[i].getBet()==count){
				return winnerPos;
			}
			winnerPos++;
		}

		return -1;
	}

	public void setBet(int bet) {
		players[0].setBet(bet);
		for (int i=1; i<players.length; i++)
			players[i].bid();
	}
}