package application;

import java.util.ArrayList;
import java.util.List;

public class ProdueixEsdeveniments {

	List<Listener> listeners = new ArrayList<Listener>();

	public void addEventListener(Listener listener){
		listeners.add(listener);
	}

	public void creaEsdeveniment(int esdeveniment){

		for (Listener l : listeners){
			l.notifyEvent(esdeveniment);
		}

	}


}
