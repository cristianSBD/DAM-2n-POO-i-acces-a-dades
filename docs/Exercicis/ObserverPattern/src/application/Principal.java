package application;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GuardaEsdeveniments guarda = new GuardaEsdeveniments();
		MostraEsdeveniments mostra = new MostraEsdeveniments(); 
		ProdueixEsdeveniments produeix = new ProdueixEsdeveniments();
		
		produeix.addEventListener(guarda);
		produeix.addEventListener(mostra);
		
		produeix.creaEsdeveniment(1);
		produeix.creaEsdeveniment(2);
		produeix.creaEsdeveniment(3);
		produeix.creaEsdeveniment(6);
		produeix.creaEsdeveniment(7);
		produeix.creaEsdeveniment(8);
		 
		guarda.toString();
		

	}

}
